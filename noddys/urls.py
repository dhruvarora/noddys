from django.conf.urls import url, include
from django.contrib import admin

urlpatterns = [
    url(r'', include("website.urls", namespace="website")),
    url(r'^photologue/', include('photologue.urls', namespace='photologue')),
    url(r'^admin/', admin.site.urls),
]
