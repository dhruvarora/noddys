# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from .models import HomepageSlider, Query, Testimonial

admin.site.register(HomepageSlider)
admin.site.register(Query)
admin.site.register(Testimonial)
