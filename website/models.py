# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.utils import timezone
from ckeditor.fields import RichTextField

class HomepageSlider(models.Model):
    date_added = models.DateField(default=timezone.now)
    title = models.CharField(max_length=100)
    image = models.FileField(max_length=150, upload_to="homepage-slider")
    content = RichTextField(blank=True, null=True)

    def __str__(self):
        return self.title

class Query(models.Model):
    date_added = models.DateTimeField(default=timezone.now)
    name = models.CharField(max_length=150)
    email = models.EmailField(max_length=150)
    mobile = models.BigIntegerField(blank=True, null=True)
    message = models.TextField(blank=True, null=True)

    def __str__(self):
        return "Query by " + self.name

class Testimonial(models.Model):
    date_added = models.DateTimeField(default=timezone.now)
    parent_name = models.CharField(max_length=150)
    child_name = models.CharField(max_length=150)
    parent_title = models.CharField(max_length=150)
    parent_image = models.FileField(upload_to="testimonials", blank=True, null=True, max_length=150)
    testimonial = models.TextField()

    def __str__(self):
        return self.parent_name
