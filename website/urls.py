from django.conf.urls import url, include
from . import views

urlpatterns = [
    url(r'^$', views.index, name="index"),
    url(r'^pages/', include('django.contrib.flatpages.urls')),
    url(r'^contact/$', views.contact, name="contact"),
    url(r'^about/$', views.about, name="about"),
    url(r'^classes/$', views.classes, name="classes"),
    url(r'^management/$', views.management, name="management"),
]
