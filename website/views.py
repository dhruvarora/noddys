# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from .models import Testimonial

def index(request):
    testimonials = Testimonial.objects.filter()[:5]

    return render(request, "index-2.html", {
    "test":testimonials,
    })

def contact(request):
    return render(request, "contact.html", {})

def about(request):
    return render(request, "about.html", {})

def classes(request):
    return render(request, "classes.html", {})

def management(request):
    return render(request, "teachers.html", {})
